<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware(['auth:sanctum'])->group(function () {
    Broadcast::routes();
    Route::get('/profile', function(Request $request){
        return $request->user();
    });
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::patch('/changePassword/{uid}', [UserController::class, 'apiChangePassword']);
    Route::get('/getUserPassword/{uid}', [UserController::class, 'getUserPassword']);
    Route::patch('/update/{uid}', [UserController::class, 'apiUpdateUser']);
    Route::post('/searchUser', [UserController::class, 'searchUser']);
});
Route::post('/register', [UserController::class, 'apiCreateUser']);
Route::post('/login', [AuthController::class, 'login']);

