<?php

namespace App\Exceptions;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class APIException extends \Exception implements HttpExceptionInterface, Responsable
{
    protected $statusCode = 500;
    protected $context = [];

    public function __construct(string $message, int $statusCode = 500, array $context = [], \Throwable|null $previous = null)
    {
        $this->statusCode = $statusCode;
        $this->context = $context;
        parent::__construct(trans($message), 0, $previous);
    }

    /**
     * Returns the status code.
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Returns response headers.
     */
    public function getHeaders(): array
    {
        return [];
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return response()->json(['message' => $this->getMessage()], $this->getStatusCode());
    }

    /**
     * Returns true when exception message is safe to be displayed to a client.
     */
    public function isClientSafe(): bool
    {
        //TODO: depends on the deploy environment instead
        return false;
    }

    /**
     * Returns string describing a category of the error.
     *
     * Value "graphql" is reserved for errors produced by query parsing or validation, do not use it.
     *
     * @api
     * @return string
     */
    public function getCategory(): string
    {
        return 'custom';
    }

    /**
     * Return the content that is put in the "extensions" part
     * of the returned error.
     *
     * @return array
     */
    public function extensionsContent(): array
    {
        return [
            'error_code' => $this->getStatusCode(),
        ];
    }
}
