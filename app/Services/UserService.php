<?php

namespace App\Services;

use App\Models\User as UserModel;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class UserService
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Create a user with random password
     * @param string $username
     * @param string $email
     * @param bool $isAdmin
     * @param bool $isDataAuthority
     * @param int $status
     * @return User
     */
    public function createUser(string $username, string $email, bool $isAdmin, int $status, int $roleID)
    {
        $password =  Str::random(8);
        $userModel = $this->userRepository->createUser($username, $email, $password, $isAdmin, $status, $roleID);
        $userModel->generated_password = $password;
        return $userModel;
    }
       /**
     * Reset password for an user with UID
     * @param string $uid
     * @return UserModel
     */
    public function resetUserPassword(string $uid)
    {
        $password = Str::random(8);
        return $this->setUserPassword($uid, $password);
    }

    /**
     * Set password for an user with UID
     * @param string $uid
     * @param string $password
     * @return UserModel
     */
    public function setUserPassword(string $uid, string $password)
    {
        /** @var UserModel $userModel */
        $userModel = $this->userRepository->findUserByUID($uid);

        if (!isset($userModel)) {
            abort(404,'User not found');
        }

        $this->userRepository->updateUserPassword($userModel->id, $password);
        $userModel->generated_password = $password;
        return $userModel;
    }
    public function findUserByUID(string $uid)
    {
        $userModel = $this->userRepository->findUserByUID($uid);
        return $userModel;
    }

    /**
     * Update user info
     * @param array $info
     */
    public function updateUser(string $uid, array $info)
    {
        /** @var UserModel $userModel */
        $userModel = $this->userRepository->updateUserInfo($uid, $info);
        return $userModel->refresh();
    }
    
    public function searchUsers(array $data)
    {
        $users = $this->userRepository->searchUsers($data);
        return $users;
    }

}
