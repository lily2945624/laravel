<?php

namespace App\Repositories;
use App\Models\User;
use App\Enums\StatusCode;
use App\Exceptions\APIException;
use App\Repositories\BaseRepository;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

use function PHPUnit\Framework\isEmpty;

class UserRepository extends BaseRepository
{
    /**
     * Create a record in "users" table
     * @param string $username
     * @param string $email
     * @param string $password
     * @param bool $isAdmin
     * @param int $status 0 or 1
     */
    public function createUser(string $username, string $email, string $password, bool $isAdmin, int $status, int $roleID)
    {
        try {
            $user = User::query()->create([
                User::UID => md5($email),
                User::USERNAME => $username,
                User::EMAIL => $email,
                User::PASSWORD => Hash::make($password),
                User::IS_ADMIN => $isAdmin,
                User::STATUS => $status,
                User::ROLE_ID => $roleID
            ]);
            return $user->refresh();
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    public function getUserByEmail(string $email){
        $user = User::query()->where(User::EMAIL, $email)->first();
        return $user;
    }

       /**
     * Find user record by UID
     * @param string $uid
     * @return null|User
     */
    public function findUserByUID(string $uid)
    {
        $userModel = User::query()->where(User::UID, $uid)->first();
        if (!isset($userModel)) {
            throw new APIException('user_not_found', StatusCode::NOT_FOUND);
        }
        return $userModel;
    }
        /**
     * Update user password
     * @param int $id user id
     * @param string password new password
     */
    public function updateUserPassword(int $id, string $password)
    {
        return User::query()->where(User::ID, $id)->update([
            User::PASSWORD => Hash::make($password)
        ]);
    }
    /**
     * Update user info
     * @param string $uid user uid
     * @param array $info
   
     */
    public function updateUserInfo(string $uid, array $info)
    {
        /** @var User $currentUser */
        $currentUser = Auth::user();
        $userModel = $this->findUserByUID($uid);

        if (count($info) > 0) {
            $userModel->update($info);
        }
        return $userModel;
    }
    public function searchUsers(array $data)
    {
        if(!empty($data))
        {
            if(array_key_exists('email', $data) && array_key_exists('username', $data))
            {
                $users = User::where([
                    [User::USERNAME, 'like', '%'.$data['username'].'%'],
                    [User::EMAIL, 'like', '%'.$data['email'].'%']                        
                ])->get();
            }
            foreach (array_keys($data) as $key) {
                $value = $data[$key];               
                switch ($key) {
                    case User::USERNAME:
                        $users = User::where(User::USERNAME, 'like', "%$value%")->get();
                        break;
                    case User::EMAIL:
                        $users = User::where(User::EMAIL, 'like', "%$value%")->get();
                        break;
                    case 'userDefinedString':
                        $users = [];
                        if (!empty($value)) {
                            foreach ($value as $value) {
                                $user = User::where(User::USERNAME, 'like', "%$value%")
                                        ->orWhere(User::EMAIL, 'like', "%$value%")->get();
                                array_push($users, $user);
                            }
                        }
                        break;
                    }
                }  
        } 
        else
        {
            $users = User::all();
        }     
        return $users;
    }

}
