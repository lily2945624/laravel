<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BaseRepository
{
    /**
     * Begin transaction, run function
     * @param \Closure $function function working with DB
     * @return mixed $function return value, null if fail
     */
    protected function transaction($function): mixed
    {
        DB::beginTransaction();
        try {
            $result = $function();
            DB::commit();
            return $result;
        } catch (\Exception $e) {
            Log::error('DB transction fail!', ['error' => $e->getMessage()]);
            DB::rollBack();
            return null;
        }
    }
}
