<?php

namespace App\Enums;

class StatusCode
{
    # The request has been accepted by host
    public const SUCCESS = 200;

    # The request has been accepted and request data has been applied on host
    public const NEW_RESOURCE = 201;

    #The request is accepted, no content is responsed
    public const NO_CONTENT = 204;

    # Bad request
    public const BAD_REQUEST = 400;

    # Unauthenticated
    public const UNAUTHENTICATED = 401;

    # Unauthorized (could not have rights to access request resource)
    public const UNAUTHORIZED = 403;

    # Request resource not found on host
    public const NOT_FOUND = 404;

    # Access not Allowed
    public const ACCESS_NOT_ALLOWED = 405;

    # Unprocessable data (request data is invalid)
    public const UNPROCESSABLE_ENTITY = 422;

    # Client error
    public const CLIENT_INPUT_ERROR = 480;

    # Internal server error
    public const INTERNAL_SERVER_ERROR = 500;

    # Datasource exception
    public const SERVER_DATASOURCE_ERROR = 570;

    # Server error
    public const SERVER_GENERIC_ERROR = 580;

    # Database exception
    public const SERVER_DB_ERROR = 590;
}
