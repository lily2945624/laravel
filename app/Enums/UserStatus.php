<?php declare(strict_types=1);

namespace App\Enums;


final class UserStatus 
{
    const DISABLE = 0;
    const ENABLE = 1;
}
