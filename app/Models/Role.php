<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    public const ID = 'id';
    public const ROLE = 'role';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
}
