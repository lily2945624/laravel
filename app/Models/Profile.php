<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    public const ID = 'id';
    public const UID = 'uuid';
    public const FIRST_NAME = 'first_name';
    public const LAST_NAME = 'last_name';
    public const ADRESS = 'adress';
    public const PHONE_NUMBER = 'phone_number';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
}
