<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use function PHPUnit\Framework\isEmpty;

class AuthController extends Controller
{
    protected UserRepository $userRepository;
    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }

    public function login(Request $request)
    {
        $user = $this->userRepository->getUserByEmail($request->email);

        if (empty($user) || !Hash::check($request->password, $user->password, [])){
            return response()->json(
                [
                    'message'=>'Wrong email or password'
                ],
                404
            );
        }

        $token = $user->createToken('authToken')->plainTextToken;

        return response()->json(
            [
                'access_token'=> $token,
                'type_token'=>'Bearer'
            ],
            200
        );
    }

    public function logout(Request $request)
    {
        // Revoke the token that was used to authenticate the current request...
        $request->user()->currentAccessToken()->delete();
        return response()->json(
            [
                'message' =>'logout'
            ],
            200
        );
    }
}
