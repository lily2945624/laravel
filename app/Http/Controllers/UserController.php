<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Services\UserService;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    protected UserService $userService;
    public function __construct(UserService $userService){
        $this->userService = $userService;
    }
    public function apiCreateUser(Request $request)
    {
        $fields = $request->validate([
            'username' => ['required', 'max:255', 'unique:users'],
            'email' => ['required', 'email', 'unique:users'],
            'is_admin' => ['required', 'boolean'],
            'status' => ['boolean'],
            'roleID'=>['integer']
        ]);

        $email = $fields['email'];
        $username = $fields['username'];
        $isAdmin = $fields['is_admin'];
        $status = $fields['status'] ?? true;
        $roleID = $fields['$roleID'] ?? true;

        try {
            DB::beginTransaction();
            /** @var User $user */
            $user = $this->userService->createUser($username, $email, $isAdmin, $status, $roleID);
            DB::commit();
            return response()->json(
                [
                    'message' =>' User created',
                    'info'=>$user
                ],
                200
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([$e->getMessage()],$e->getCode());
        }
    }

    public function apiChangePassword(Request $request, string $uid)
    {
        $fields = $request->validate([
            'old_password' => ['string', 'bail', 'required'],
            'password' => ['string', 'bail', 'required', 'different:old_password', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', 'min:8', 'max:16'],
            'password_confirmation' => ['required', 'same:password']
        ]);

        $oldPassword = $fields['old_password'];
        $password = $fields['password'];

        try {
            $userModel = $this->userService->findUserByUID($uid);

            # Confirm old password
            if (!Hash::check($oldPassword, $userModel->password)) {
                return response()->json(['message'=>'old password is not correct'],404);
            }
            $user = $this->userService->setUserPassword($uid, $password);
            return response()->json(['message'=>'password changed'],200);
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()],$e->getCode());
        }
    }
    public function apiUpdateUser(Request $request, string $uid)
    {
        Log::debug(__FUNCTION__, $request->all());

        $fields = $request->validate([
            'username' => ['string'],
            'email' => ['email', Rule::unique('users')->ignore($uid, 'uuid')],
            'is_admin' => ['boolean'],
            'status' => ['boolean']
        ]);

        if (array_key_exists('username', $fields)) {
            $request->validate([
                'username' => ['max:255', Rule::unique('users')->ignore($uid, 'uuid'), 'required'],
            ]);
        }

        try {
            DB::beginTransaction();
            /** @var User $user */
            $user = $this->userService->updateUser($uid, $fields);

            DB::commit();
            return response()->json(
                [
                    'message' =>'User updated',
                    'return'=> $user
                ],
                200
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([$e->getMessage()],$e->getCode());
        }
    }

    public function searchUser(Request $request)
    {
        
        $fields = $request->validate([
            'username' => ['string','min:1', 'max:255'],
            'email' => ['string','min:1', 'max:255'],
            'userDefinedString' =>['array'],
        ]);
        try{
            $users = $this->userService->searchUsers($fields);
            return response()->json(
                [
                    'result'=> $users,
                ],
                200
            );
        } catch(\Exception $e)
        {
            return response()->json([$e->getMessage()],$e->getCode());
        }
    }
}
